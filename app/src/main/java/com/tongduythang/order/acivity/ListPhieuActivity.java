package com.tongduythang.order.acivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.tongduythang.order.R;
import com.tongduythang.order.adapter.PhieuCustomListViewAdapter;
import com.tongduythang.order.database.PhieuOrderDatabaseSqliteHelper;
import com.tongduythang.order.models.Phieu;

import java.util.ArrayList;
import java.util.List;

public class ListPhieuActivity extends AppCompatActivity {
    private ListView listView;
    private PhieuCustomListViewAdapter adapter;
    private PhieuOrderDatabaseSqliteHelper databaseSqliteHelper;
    private List<Phieu> listPhieu = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list);

        // init view
        initView();

        // init data for listview
        initDataListView();
    }

    private void initDataListView() {
        databaseSqliteHelper = new PhieuOrderDatabaseSqliteHelper(this);
        listPhieu = databaseSqliteHelper.getAllPhieu();

        adapter = new PhieuCustomListViewAdapter(this, listPhieu);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void initView() {
        listView = findViewById(R.id.lvMonAn);
    }
}