package com.tongduythang.order.acivity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.tongduythang.order.R;
import com.tongduythang.order.adapter.PhieuCustomListViewAdapter;
import com.tongduythang.order.database.PhieuOrderDatabaseSqliteHelper;
import com.tongduythang.order.models.Phieu;
import com.tongduythang.order.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private PhieuOrderDatabaseSqliteHelper databaseSqliteHelper;
    private Button buttonSave;
    private Button buttonView;
    private EditText editTextName, editTextID, editTextGia;
    private CheckBox checkBoxIsGiaVi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init view
        initView();

        // init database
        databaseSqliteHelper = new PhieuOrderDatabaseSqliteHelper(this);
        // handler click
        buttonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListPhieuActivity.class);
                startActivity(intent);
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextName.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Tên không được để trống!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(editTextGia.getText().toString().trim().isEmpty()){
                    Toast.makeText(MainActivity.this, "Giá không được để trống!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!Utils.isNumber(editTextGia.getText().toString())){
                    Toast.makeText(MainActivity.this, "Giá phải là số!", Toast.LENGTH_SHORT).show();
                    return;
                }
                boolean resultCheckBox = checkBoxIsGiaVi.isChecked();
                Phieu phieu = new Phieu(0, editTextName.getText().toString(), Integer.parseInt(editTextGia.getText().toString()), resultCheckBox);
                boolean result = databaseSqliteHelper.themPhieu(phieu);
                if(result){
                    Toast.makeText(MainActivity.this, "Thêm thành công", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Thêm thất bại", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private void initView() {
        buttonSave = findViewById(R.id.btnSave);
        buttonView = findViewById(R.id.btnView);

        editTextID = findViewById(R.id.ed_id);
        editTextName = findViewById(R.id.ed_name);
        editTextGia = findViewById(R.id.ed_gia);
        checkBoxIsGiaVi = findViewById(R.id.checkboxGiaVi);
    }
}