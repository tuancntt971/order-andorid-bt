package com.tongduythang.order.models;

public class Phieu {
    private int id;
    private String name;
    private int price;
    private boolean isGiaVi;

    public Phieu(int id, String name, int price, boolean isGiaVi) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.isGiaVi = isGiaVi;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setGiaVi(boolean giaVi) {
        isGiaVi = giaVi;
    }

    public boolean isGiaVi() {
        return isGiaVi;
    }
}
