package com.tongduythang.order.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.tongduythang.order.models.Phieu;

import java.util.ArrayList;
import java.util.List;

public class PhieuOrderDatabaseSqliteHelper extends SQLiteOpenHelper {
    public static final String TAG = "OrderDatabaseSqliteHelper";
    public static final String DATABASE_NAME = "order_db";
    public static final String TABLE_ORDER = "orders";
    public static final String COLUMN_ORDER_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_IS_GIA_VI = "is_gia_vi";
    public static final int DATABASE_VERSION = 1;

    public PhieuOrderDatabaseSqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Script to create table.
        String script = "CREATE TABLE " + TABLE_ORDER + "("
                + COLUMN_ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_NAME + " TEXT,"
                + COLUMN_PRICE+ " INTEGER DEFAULT 0,"
                + COLUMN_IS_GIA_VI + " INTEGER DEFAULT 0" + ")";

        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDER);

        // Create tables again
        onCreate(db);
    }

    public boolean deletePhieu(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.delete(TABLE_ORDER, "COLUMN_ORDER_ID " + "= ?", new String[] { String.valueOf(id) });
        db.close();
        return result != -1;
    }

    public boolean updatePhieu(Phieu phieu){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, phieu.getName());
        values.put(COLUMN_PRICE, phieu.getPrice());
        values.put(COLUMN_IS_GIA_VI, phieu.isGiaVi() ? 1 : 0);
        long result = db.update(TABLE_ORDER,  values, COLUMN_ORDER_ID + " = ?",
                new String[]{String.valueOf(phieu.getId())});
        db.close();
        return result != -1;
    }

    public boolean themPhieu(Phieu phieu){
        try{
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, phieu.getName());
            values.put(COLUMN_PRICE, phieu.getPrice());
            values.put(COLUMN_IS_GIA_VI, phieu.isGiaVi() ? 1 : 0);
            long result = db.insert(TABLE_ORDER, null, values);
            db.close();
            return result != -1;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public List<Phieu> getAllPhieu(){
        List<Phieu> phieus = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_ORDER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor != null && cursor.moveToFirst()) {
            do {
                Phieu phieu = new Phieu(cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getInt(2), cursor.getInt(3) != 0);
                // Adding phieu to list
                phieus.add(phieu);
            } while (cursor.moveToNext());
            cursor.close();
            db.close();
        }
        return phieus;
    }
}
