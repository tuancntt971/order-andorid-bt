package com.tongduythang.order.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.tongduythang.order.R;
import com.tongduythang.order.models.Phieu;

import java.util.List;

public class PhieuCustomListViewAdapter extends ArrayAdapter<Phieu> {
    private Context context;
    private List<Phieu> phieus;


    public PhieuCustomListViewAdapter(@NonNull Context context, List<Phieu> phieuList) {
        super(context, R.layout.item_phieu);
        this.context = context;
        this.phieus = phieuList;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;

        // we call an if statement on our view that is passed in,
        // to see if it has been recycled or not. if it has been recycled
        // then it already exists and we do not need to call the inflater function
        // this saves us a HUGE amount of resources and processing
        if(row == null){
            row = LayoutInflater.from(context).inflate(R.layout.item_phieu, null);
        }

        Phieu phieu = phieus.get(position);
        if(phieu != null){
            TextView tvID = (TextView) row.findViewById(R.id.tv_id);
            TextView tvName = (TextView) row.findViewById(R.id.tv_name);
            TextView tvPrice = (TextView) row.findViewById(R.id.tv_price);
            TextView tvIsGiaVi = (TextView) row.findViewById(R.id.tv_gia_vi);

            tvID.setText(String.format("ID: %d", phieu.getId()));
            tvName.setText(String.format("Tên: %s", phieu.getName()));
            tvPrice.setText(String.format("Giá: %d", phieu.getPrice()));
            tvIsGiaVi.setText(String.format("Gia vị: %b", phieu.isGiaVi()));
        }

        return row;
    }

    @Override
    public int getCount() {
        return phieus.size();
    }
}
